/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.journals.rest;

import com.sapient.assignment.exception.InputDoesNotExistException;
import com.sapient.assignment.exception.InvalidInputException;
import java.util.concurrent.ExecutionException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ExceptionHandler implements ExceptionMapper<Throwable> {

    @Override
    public Response toResponse(Throwable exception) {
        if (exception.getClass().equals(ExecutionException.class) && exception.getCause().getClass().equals(InputDoesNotExistException.class)) {
            return Response.status(Response.Status.NOT_FOUND).header("x-header", exception.getMessage()).build();
        }
        if (exception.getClass().equals(ExecutionException.class) && exception.getCause().getClass().equals(InvalidInputException.class)) {
            return Response.status(Response.Status.BAD_REQUEST).header("x-header", exception.getMessage()).build();
        }
        exception.printStackTrace();
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).header("x-header", exception.getMessage()).build();
    }

}
