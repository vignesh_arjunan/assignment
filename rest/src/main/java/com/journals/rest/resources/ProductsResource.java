package com.journals.rest.resources;

import com.journals.rest.ApplicationConfiguration;
import com.sapient.assignment.BatchReader;
import com.sapient.assignment.Creator;
import com.sapient.assignment.Reader;
import com.sapient.assignment.Remover;
import com.sapient.assignment.Searcher;
import com.sapient.assignment.Updater;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("products")
public class ProductsResource {

    @Resource
    ManagedExecutorService mes;

    @Inject
    private ApplicationConfiguration applicationConfiguration;

    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Path("{product}/{batch_number}/{batch_size}")
    public void getProductsInBatches(@PathParam("product") String product,
            @PathParam("batch_number") int batch_number,
            @PathParam("batch_size") int batch_size,
            @Suspended final AsyncResponse asyncResponse) throws ExecutionException, InterruptedException {

        Supplier<Response> supplier = () -> {
            BatchReader batchReader = applicationConfiguration.getBatchReaderMap().get(product);
            if (batchReader != null) {
                return Response
                        .status(Status.OK)
                        .entity(batchReader.readBatch(batch_number, batch_size))
                        .build();
            }

            return Response
                    .status(Status.NOT_FOUND)
                    .entity(product + " not found")
                    .build();
        };

        CompletableFuture.supplyAsync(supplier, mes).thenAccept(asyncResponse::resume).get();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Path("{product}/{id}")
    public void getProductById(@PathParam("product") String product,
            @PathParam("id") String id,
            @Suspended final AsyncResponse asyncResponse) throws ExecutionException, InterruptedException {

        Supplier<Response> supplier = () -> {
            Reader reader = applicationConfiguration.getReaderMap().get(product);
            if (reader != null) {
                return Response
                        .status(Status.OK)
                        .entity(reader.read("{\"id\":\"" + id + "\"}"))
                        .build();
            }

            return Response
                    .status(Status.NOT_FOUND)
                    .entity(product + " not found")
                    .build();
        };

        CompletableFuture.supplyAsync(supplier, mes).thenAccept(asyncResponse::resume).get();

    }

    @DELETE
    @Path("{product}/{id}")
    public void deleteProductById(@PathParam("product") String product,
            @PathParam("id") String id,
            @Suspended final AsyncResponse asyncResponse) throws ExecutionException, InterruptedException {

        Supplier<Response> supplier = () -> {
            Remover remover = applicationConfiguration.getRemoverMap().get(product);
            if (remover != null) {
                remover.remove("{\"id\":\"" + id + "\"}");
                return Response
                        .status(Status.OK)
                        .entity("Removed Product")
                        .build();
            }

            return Response
                    .status(Status.NOT_FOUND)
                    .entity(product + " not found")
                    .build();
        };

        CompletableFuture.supplyAsync(supplier, mes).thenAccept(asyncResponse::resume).get();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Path("{product}/search/{text}")
    public void searchProductsByText(@PathParam("product") String product,
            @PathParam("text") String text,
            @Suspended final AsyncResponse asyncResponse) throws ExecutionException, InterruptedException {

        Supplier<Response> supplier = () -> {
            Searcher searcher = applicationConfiguration.getSearcherMap().get(product);
            if (searcher != null) {
                return Response
                        .status(Status.OK)
                        .entity(searcher.search(text))
                        .build();
            }

            return Response
                    .status(Status.NOT_FOUND)
                    .entity(product + " not found")
                    .build();
        };

        CompletableFuture.supplyAsync(supplier, mes).thenAccept(asyncResponse::resume).get();
    }

    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("{product}")
    public void createProduct(@PathParam("product") String product,
            String json,
            @Suspended final AsyncResponse asyncResponse) throws ExecutionException, InterruptedException {

        Supplier<Response> supplier = () -> {
            Creator creator = applicationConfiguration.getCreatorMap().get(product);
            if (creator != null) {
                return Response
                        .status(Status.OK)
                        .entity(creator.create(json))
                        .build();
            }

            return Response
                    .status(Status.NOT_FOUND)
                    .entity(product + " not found")
                    .build();
        };

        CompletableFuture.supplyAsync(supplier, mes).thenAccept(asyncResponse::resume).get();
    }

    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PUT
    @Path("{product}")
    public void updateProduct(@PathParam("product") String product,
            String json,
            @Suspended final AsyncResponse asyncResponse) throws ExecutionException, InterruptedException {

        Supplier<Response> supplier = () -> {
            Updater updater = applicationConfiguration.getUpdaterMap().get(product);
            if (updater != null) {
                return Response
                        .status(Status.OK)
                        .entity(updater.update(json))
                        .build();
            }

            return Response
                    .status(Status.NOT_FOUND)
                    .entity(product + " not found")
                    .build();
        };

        CompletableFuture.supplyAsync(supplier, mes).thenAccept(asyncResponse::resume).get();
    }
}
