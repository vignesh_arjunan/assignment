package com.journals.rest;

import com.sapient.assignment.BatchReader;
import com.sapient.assignment.Creator;
import com.sapient.assignment.Reader;
import com.sapient.assignment.Remover;
import com.sapient.assignment.Searcher;
import com.sapient.assignment.Updater;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ServiceLoader;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("resources")
@ApplicationScoped
public class ApplicationConfiguration extends Application {

    private final Map<String, BatchReader> batchReaderMap = new HashMap<>();
    private final Map<String, Reader> readerMap = new HashMap<>();
    private final Map<String, Creator> creatorMap = new HashMap<>();
    private final Map<String, Remover> removerMap = new HashMap<>();
    private final Map<String, Searcher> searcherMap = new HashMap<>();
    private final Map<String, Updater> updaterMap = new HashMap<>();

    public Map<String, BatchReader> getBatchReaderMap() {
        return batchReaderMap;
    }

    public Map<String, Reader> getReaderMap() {
        return readerMap;
    }

    public Map<String, Creator> getCreatorMap() {
        return creatorMap;
    }

    public Map<String, Remover> getRemoverMap() {
        return removerMap;
    }

    public Map<String, Updater> getUpdaterMap() {
        return updaterMap;
    }

    public Map<String, Searcher> getSearcherMap() {
        return searcherMap;
    }

    @PostConstruct
    public void init() {
        Iterator<BatchReader> iteratorBatchReader = ServiceLoader.load(BatchReader.class).iterator();
        while (iteratorBatchReader.hasNext()) {
            BatchReader batchReader = iteratorBatchReader.next();
            batchReaderMap.put(batchReader.databaseName(), batchReader);
        }

        Iterator<Reader> iteratorReader = ServiceLoader.load(Reader.class).iterator();
        while (iteratorReader.hasNext()) {
            Reader reader = iteratorReader.next();
            readerMap.put(reader.databaseName(), reader);
        }

        Iterator<Remover> iteratorRemover = ServiceLoader.load(Remover.class).iterator();
        while (iteratorRemover.hasNext()) {
            Remover remover = iteratorRemover.next();
            removerMap.put(remover.databaseName(), remover);
        }

        Iterator<Creator> iteratorCreator = ServiceLoader.load(Creator.class).iterator();
        while (iteratorCreator.hasNext()) {
            Creator creator = iteratorCreator.next();
            creatorMap.put(creator.databaseName(), creator);
        }

        Iterator<Searcher> iteratorSearcher = ServiceLoader.load(Searcher.class).iterator();
        while (iteratorSearcher.hasNext()) {
            Searcher searcher = iteratorSearcher.next();
            searcherMap.put(searcher.databaseName(), searcher);
        }
        
        Iterator<Updater> iteratorUpdater = ServiceLoader.load(Updater.class).iterator();
        while (iteratorUpdater.hasNext()) {
            Updater updater = iteratorUpdater.next();
            updaterMap.put(updater.databaseName(), updater);
        }
    }
}
