package com.journals.rest.resource;

import java.io.IOException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ProductsResourceTest {

    private final String basePath = "http://localhost:8080/rest/resources";

    public ProductsResourceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        // TODO setup data for tests
    }

    @After
    public void tearDown() {
    }

    @Test
    public void updateProduct() throws IOException {
        JSONObject json = new JSONObject();
        json.put("id", "57221a6a148ade466cff6ab5");
        json.put("title", "Mla Handbook for Writers of Research Papers");
        json.put("author", "Milllla");
        json.put("pages", 200);
        json.put("tags", new String[]{"FICTION"});
        json.put("publisher", "Affiliated East-West Press; 7 th FOR SALE IN INDIA edition (1 December 2008)");
        Client client = ClientBuilder.newClient();
        String response = client
                .target(basePath).path("products/books/")
                .request()
                .put(Entity.entity(json.toString(), MediaType.APPLICATION_JSON), String.class);
        System.out.println("response " + response);
        assertTrue(new JSONObject(response).has("author") && new JSONObject(response).getString("author").equals("Milllla"));
        client.close();
    }

    @Test
    public void deleteProduct() throws IOException {
        JSONObject json = new JSONObject();
        json.put("id", "57221a6a148ade466cff6ab5");
        json.put("title", "Mla Handbook for Writers of Research Papers");
        json.put("author", "Milllla");
        json.put("pages", 200);
        json.put("tags", new String[]{"FICTION"});
        json.put("publisher", "Affiliated East-West Press; 7 th FOR SALE IN INDIA edition (1 December 2008)");
        Client client = ClientBuilder.newClient();
        String response = client
                .target(basePath).path("products/books/57221a6a148ade466cff6ab5")
                .request()
                .delete().toString();
        System.out.println("response " + response);
//        assertTrue(new JSONObject(response).has("author") && new JSONObject(response).getString("author").equals("Milllla"));
        client.close();
    }
}
