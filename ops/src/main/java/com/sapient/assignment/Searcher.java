package com.sapient.assignment;

/**
 *
 * @author vignesh
 */
@FunctionalInterface
public interface Searcher {

    /**
     * Returns the DB Name
     *
     * @return
     */
    default
    String databaseName() {
        return "sample";
    }

    /**
     * performs a full text search and returns the matching documents
     *
     * @param text input
     * @return matching documents
     */
    String search(String text);
}
