package com.sapient.assignment.exception;

/**
 *
 * @author vignesh
 */
public class InputDoesNotExistException extends RuntimeException {

    public InputDoesNotExistException() {
    }

    public InputDoesNotExistException(String message) {
        super(message);
    }

    public InputDoesNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

}
