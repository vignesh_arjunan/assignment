package com.sapient.assignment;

import com.sapient.assignment.exception.InputDoesNotExistException;
import com.sapient.assignment.exception.InvalidInputException;

/**
 *
 * @author vignesh
 */
@FunctionalInterface
public interface Remover {

    /**
     * Returns the DB Name
     *
     * @return
     */
    default
    String databaseName() {
        return "sample";
    }

    /**
     * reads the input for id and deletes the document
     *
     * @param json
     */
    void remove(String json) throws InvalidInputException, InputDoesNotExistException;
}
