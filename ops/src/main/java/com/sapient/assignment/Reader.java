package com.sapient.assignment;

import com.sapient.assignment.exception.InputDoesNotExistException;
import com.sapient.assignment.exception.InvalidInputException;

/**
 *
 * @author vignesh
 */
@FunctionalInterface
public interface Reader {

    /**
     * Returns the DB Name
     *
     * @return
     */
    default
    String databaseName() {
        return "sample";
    }

    /**
     * reads the input for id and returns the whole document
     *
     * @param json
     * @return
     */
    String read(String json) throws InvalidInputException, InputDoesNotExistException;
}
