package com.sapient.assignment;

import com.sapient.assignment.exception.InvalidInputException;

/**
 *
 * @author vignesh
 */
@FunctionalInterface
public interface BatchReader {

    /**
     * Returns the DB Name
     *
     * @return
     */
    default
    String databaseName() {
        return "sample";
    }

    /**
     * reads the inputs and returns an array document
     *
     * @param json
     * @return
     */
    String readBatch(int batchNumber, int batchSize) throws InvalidInputException ;    
}
