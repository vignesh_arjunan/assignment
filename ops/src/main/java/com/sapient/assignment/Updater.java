package com.sapient.assignment;

import com.sapient.assignment.exception.InputDoesNotExistException;
import com.sapient.assignment.exception.InvalidInputException;

/**
 *
 * @author vignesh
 */
@FunctionalInterface
public interface Updater {

    /**
     * Returns the DB Name
     *
     * @return
     */
    default
    String databaseName() {
        return "sample";
    }

    /**
     * reads the input for id and updates the document with the input and
     * returns the same
     *
     * @param json input doc
     * @return updated doc
     */
    String update(String json) throws InvalidInputException, InputDoesNotExistException;
}
