package com.sapient.assignment;

import com.sapient.assignment.exception.InvalidInputException;

/**
 *
 * @author vignesh
 */
@FunctionalInterface
public interface Creator {

    /**
     * Returns the DB Name
     *
     * @return
     */
    default
    String databaseName() {
        return "sample";
    }

    /**
     * creates a record with the input JSON and returns the same with the id
     *
     * @param json input doc
     * @return created doc
     */
    String create(String json) throws InvalidInputException;
}
