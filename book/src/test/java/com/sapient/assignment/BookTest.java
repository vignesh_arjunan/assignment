package com.sapient.assignment;

import com.mongodb.MongoClient;
import com.sapient.assignment.Book.Tag;
import static com.sapient.assignment.Book.Tag.DRAMA;
import static com.sapient.assignment.Book.Tag.FICTION;
import static com.sapient.assignment.Book.Tag.HUMOUR;
import static com.sapient.assignment.Database.DB_NAME;
import static com.sapient.assignment.Database.MAPPER;
import java.io.IOException;
import java.util.List;
import java.util.ServiceLoader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;

/**
 *
 * @author vignesh
 */
public class BookTest {

    public BookTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        // clearing all data first
        try (MongoClient mongoClient = Database.getMongoClient()) {

            // create the Datastore connecting to the default port on the local host
            final Datastore datastore = Database.getMorphia().createDatastore(mongoClient, DB_NAME);
            datastore.ensureIndexes(false);

            final Query<Book> all = datastore.createQuery(Book.class);
            datastore.delete(all);
        }
    }

    @After
    public void tearDown() {
    }

    @Test
    public void creatorReaderUpdaterRemoverTest() throws IOException {

        Creator creator = ServiceLoader.load(Creator.class).iterator().next();
        Book book = MAPPER.readValue(creator.create("{\n"
                + "	\"title\" : \"Runaway Writers\",\n"
                + "	\"author\" : \"Indu Balachandran\",\n"
                + "	\"pages\" : 123,\n"
                + "	\"tags\" : [\n"
                + "		\"HUMOUR\",\n"
                + "		\"DRAMA\"\n"
                + "	],\n"
                + "	\"publisher\" : \"Speaking Tiger (15 December 2015)\"\n"
                + "        }"), Book.class);

        System.out.println("book " + book);

        Book onlyId = new Book();
        onlyId.setId(book.getId());

        Reader reader = ServiceLoader.load(Reader.class).iterator().next();
        onlyId = MAPPER.readValue(reader.read(MAPPER.writeValueAsString(onlyId)), Book.class);
        onlyId.setPages(onlyId.getPages() * 2);
        onlyId.setTags(new Tag[]{FICTION});

        Updater updater = ServiceLoader.load(Updater.class).iterator().next();
        onlyId = MAPPER.readValue(reader.read(updater.update(MAPPER.writeValueAsString(onlyId))), Book.class);
        Assert.assertEquals(246, onlyId.getPages());

        Remover remover = ServiceLoader.load(Remover.class).iterator().next();
        remover.remove(MAPPER.writeValueAsString(onlyId));

        try (MongoClient mongoClient = Database.getMongoClient()) {

            // create the Datastore connecting to the default port on the local host
            final Datastore datastore = Database.getMorphia().createDatastore(mongoClient, DB_NAME);
            datastore.ensureIndexes(false);

            List<Book> good = datastore.createQuery(Book.class)
                    .order("pages")
                    .asList();
            good.forEach(System.out::println);
            Assert.assertEquals(0, good.size());
        }
    }

    @Test
    public void insertDataAndSearch() throws IOException {

        try (MongoClient mongoClient = Database.getMongoClient()) {

            // create the Datastore connecting to the default port on the local host
            final Datastore datastore = Database.getMorphia().createDatastore(mongoClient, DB_NAME);
            datastore.ensureIndexes(false);

            Book book = new Book("The Great Gatsby (Everyman)", "F. Scott Fitzgerald", 224, "Phoenix; New edition edition (1 June 1993)");
            book.setTags(new Tag[]{HUMOUR});
            datastore.save(book);
            System.out.println("added book " + book);
            book = new Book("Runaway Writers", "Indu Balachandran", 123, "Speaking Tiger (15 December 2015)");
            book.setTags(new Tag[]{HUMOUR, DRAMA});
            datastore.save(book);
            System.out.println("added book " + book);
            book = new Book("A Writer's Nightmare", "R.K. Narayan", 240, "Penguin India (14 October 2000)");
            book.setTags(new Tag[]{FICTION});
            datastore.save(book);
            System.out.println("added book " + book);
            book = new Book("Mla Handbook for Writers of Research Papers", "Mla", 200, "Affiliated East-West Press; 7 th FOR SALE IN INDIA edition (1 December 2008)");
            book.setTags(new Tag[]{FICTION});
            datastore.save(book);
            System.out.println("added book " + book);
        }

        Searcher searcher = ServiceLoader.load(Searcher.class).iterator().next();
        searcher.search(HUMOUR.toString());
        System.out.println("searcher.search(HUMOUR.toString()) " + searcher.search(HUMOUR.toString()));
        List<Book> books = MAPPER.readValue(searcher.search(HUMOUR.toString()), MAPPER.getTypeFactory().constructCollectionType(List.class, Book.class));
        Assert.assertEquals(2, books.size());

        BatchReader batchReader = ServiceLoader.load(BatchReader.class).iterator().next();
        books = MAPPER.readValue(batchReader.readBatch(2, 2), MAPPER.getTypeFactory().constructCollectionType(List.class, Book.class));
        Assert.assertEquals(2, books.size());

        books = MAPPER.readValue(batchReader.readBatch(3, 2), MAPPER.getTypeFactory().constructCollectionType(List.class, Book.class));
        Assert.assertEquals(0, books.size());
    }
}
