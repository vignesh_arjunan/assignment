package com.sapient.assignment;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mongodb.MongoClient;
import static com.sapient.assignment.Database.DB_NAME;
import static com.sapient.assignment.Database.MAPPER;
import com.sapient.assignment.exception.InputDoesNotExistException;
import com.sapient.assignment.exception.InvalidInputException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.UpdateOperations;

/**
 *
 * @author vignesh
 */
public class BookUpdater implements Updater {

    @Override
    public String databaseName() {
        return Database.DB_NAME;
    }

    @Override
    public String update(String json) throws InvalidInputException, InputDoesNotExistException {
        Book book;
        try {
            book = MAPPER.readValue(json, Book.class);

            if (book.getId() == null) {
                throw new IllegalArgumentException("id not set");
            }

            try (MongoClient mongoClient = Database.getMongoClient()) {
                // create the Datastore connecting to the default port on the local host
                final Datastore datastore = Database.getMorphia().createDatastore(mongoClient, DB_NAME);
                datastore.ensureIndexes(false);
                if (datastore.find(Book.class).field("_id").equal(book.getId()).get() == null) {
                    throw new InputDoesNotExistException("id does not exist");
                }
                UpdateOperations<Book> updateOperations = datastore.createUpdateOperations(Book.class);
                updateOperations.set("author", book.getAuthor());
                updateOperations.set("publisher", book.getPublisher());
                updateOperations.set("pages", book.getPages());
                updateOperations.set("tags", book.getTags());
                updateOperations.set("title", book.getTitle());
                datastore.update(book, updateOperations);
            }
            return MAPPER.writeValueAsString(book);

        } catch (JsonParseException | JsonMappingException ex) {
            Logger.getLogger(BookCreator.class.getName()).log(Level.SEVERE, null, ex);
            throw new InvalidInputException("json is not good", ex);
        } catch (IOException ex) {
            Logger.getLogger(BookCreator.class.getName()).log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }
}
