package com.sapient.assignment;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.stream.Stream;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;
import org.mongodb.morphia.utils.IndexType;

/**
 *
 * @author vignesh
 */
@Entity
@Indexes(
        @Index(fields = @Field(value = "$**", type = IndexType.TEXT)))
public class Book implements Serializable {

    @Id
    @JsonSerialize(using = ObjectIdSerializer.class)
    private ObjectId id;
    private String title;
    private String author;
    private int pages;
    private Tag[] tags;
    private String publisher;

    public static enum Tag {
        HUMOUR, FICTION, DRAMA
    }

    public Book() {

    }

    public Book(String title, String author, int pages, String publisher) {
        this.title = title;
        this.author = author;
        this.pages = pages;
        this.publisher = publisher;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public Tag[] getTags() {
        return tags;
    }

    public void setTags(Tag[] tags) {
        this.tags = tags;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        String tagString = "";
        if (tags != null) {
            tagString = Stream.of(tags).collect(StringBuilder::new,
                    (StringBuilder str, Tag t) -> {
                        str.append(t.toString());
                    },
                    (StringBuilder s1, StringBuilder s2) -> {
                        s1.append(s2);
                    }
            ).toString();
        }
        return "Book{" + "id=" + id + ", title=" + title + ", author=" + author + ", pages=" + pages + ", tags=[" + tagString + "], publisher=" + publisher + "}";
    }

}
