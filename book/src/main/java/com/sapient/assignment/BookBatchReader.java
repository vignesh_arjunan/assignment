package com.sapient.assignment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.MongoClient;
import static com.sapient.assignment.Database.DB_NAME;
import static com.sapient.assignment.Database.MAPPER;
import com.sapient.assignment.exception.InvalidInputException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.mongodb.morphia.Datastore;

/**
 *
 * @author vignesh
 */
public class BookBatchReader implements BatchReader {

    @Override
    public String databaseName() {
        return Database.DB_NAME;
    }

    @Override
    public String readBatch(int batchNumber, int batchSize) throws InvalidInputException {
        try {
            if (batchNumber < 0 || batchSize < 0) {
                throw new InvalidInputException("inputs should not be -ve");
            }

            try (MongoClient mongoClient = Database.getMongoClient()) {
                // create the Datastore connecting to the default port on the local host
                final Datastore datastore = Database.getMorphia().createDatastore(mongoClient, DB_NAME);
                datastore.ensureIndexes(false);
                return MAPPER.writeValueAsString(
                        datastore.createQuery(Book.class)
                        .offset((batchNumber - 1) * batchSize)
                        .limit(batchSize)
                        .order("id")
                        .asList()
                );
            }

        } catch (JsonProcessingException ex) {
            Logger.getLogger(BookCreator.class.getName()).log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

}
