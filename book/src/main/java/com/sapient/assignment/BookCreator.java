package com.sapient.assignment;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mongodb.MongoClient;
import static com.sapient.assignment.Database.DB_NAME;
import static com.sapient.assignment.Database.MAPPER;
import com.sapient.assignment.exception.InvalidInputException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.mongodb.morphia.Datastore;

/**
 *
 * @author vignesh
 */
public class BookCreator implements Creator {

    @Override
    public String databaseName() {
        return Database.DB_NAME;
    }

    @Override
    public String create(String json) throws InvalidInputException {
        Book book;
        try {
            book = MAPPER.readValue(json, Book.class);
            try (MongoClient mongoClient = Database.getMongoClient()) {
                // create the Datastore connecting to the default port on the local host
                final Datastore datastore = Database.getMorphia().createDatastore(mongoClient, DB_NAME);
                datastore.ensureIndexes(false);
                datastore.save(book);
            }
            return MAPPER.writeValueAsString(book);
        } catch (JsonParseException | JsonMappingException ex) {
            Logger.getLogger(BookCreator.class.getName()).log(Level.SEVERE, null, ex);
            throw new InvalidInputException("json is not good", ex);
        } catch (IOException ex) {
            Logger.getLogger(BookCreator.class.getName()).log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }
}
