package com.sapient.assignment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.MongoClient;
import static com.sapient.assignment.Database.DB_NAME;
import static com.sapient.assignment.Database.MAPPER;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.mongodb.morphia.Datastore;

/**
 *
 * @author vignesh
 */
public class BookSearcher implements Searcher {

    @Override
    public String databaseName() {
        return Database.DB_NAME;
    }

    @Override
    public String search(String text) {
        try (MongoClient mongoClient = Database.getMongoClient()) {

            // create the Datastore connecting to the default port on the local host
            final Datastore datastore = Database.getMorphia().createDatastore(mongoClient, DB_NAME);
            datastore.ensureIndexes(false);

            List<Book> good = datastore.createQuery(Book.class)
                    .search(text)
                    .order("id")
                    .asList();
            return MAPPER.writeValueAsString(good);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(BookSearcher.class.getName()).log(Level.SEVERE, null, ex);
            throw new IllegalStateException("Could not serialize docs", ex);
        }
    }

}
