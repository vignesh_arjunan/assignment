package com.sapient.assignment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import java.util.Optional;
import org.mongodb.morphia.Morphia;

/**
 *
 * @author vignesh
 */
public class Database {

    public static String PACKAGE = "com.sapient.assignment";
    public static String DB_NAME = "books";
    public static final ObjectMapper MAPPER = new ObjectMapper();

    public static Morphia getMorphia() {
        Morphia morphia = new Morphia();
        morphia.mapPackage(PACKAGE);
        return morphia;
    }

    public static MongoClient getMongoClient() {
        return new MongoClient(Optional.ofNullable(System.getProperty("DB_HOST")).orElse("localhost"));
    }
}
